# CHANGELOG

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## [1.0.1] - 2019-12-17
## Fixed
- ci yaml change to not have old configs

## [1.0.0] - 2019-12-17
## Added
- aapg with yaml configs
